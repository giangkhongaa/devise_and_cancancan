class Ability
  include CanCan::Ability

    def initialize(user)
        user ||= User.new
        if user.has_role? :admin
            can :manage, :all # admin có tất cả các quyền
        elsif user.has_role? :user
            can :create, Article # user can create Article
            can [:update,:destroy], Article do |article|
                article.try(:user) == user # User chỉ có thể update và destroy article của chính mình.
            end
            can :read, :all
        else
            can :read, :all
        end
    end
end
