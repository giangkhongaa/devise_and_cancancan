class ApplicationController < ActionController::Base
 	protect_from_forgery with: :exception
 	rescue_from CanCan::AccessDenied do |exception|									
  		render :file => 'public/500.html', :status => :denied, :layout => false								
  	end									


  	before_action :configure_permitted_parameters, if: :devise_controller?
  	
  	protected
    	def configure_permitted_parameters
    		devise_parameter_sanitizer.permit(:sign_up, keys: [:email,:password, :password_confirmation, roles: []])
    	end
end
