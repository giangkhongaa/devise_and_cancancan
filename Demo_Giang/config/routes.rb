Rails.application.routes.draw do
 

  resources :articles
  devise_for :users
  scope path: "/welcome", controller: :welcome, as: "hi" do
	  get 'hello' => :hello
	  get 'index' => :index
  end
  root to: "articles#index"
end
